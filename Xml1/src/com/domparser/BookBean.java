package com.domparser;

import java.util.ArrayList;
import java.util.List;


public class BookBean {

	private String bookType;
	private String bookTitle;
	private String bookIsbn;

	private List<String> authList = new ArrayList<String>();

	public List<String> getAuthList() {
		return authList;
	}

	public void setAuthList(List<String> authList) {
		this.authList = authList;
	}

	public String getBookType() {
		return bookType;
	}

	public void setBookType(String bookType) {
		this.bookType = bookType;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public String getBookIsbn() {
		return bookIsbn;
	}

	public void setBookIsbn(String bookIsbn) {
		this.bookIsbn = bookIsbn;
	}

	public void addAuthor(String authName) {
		this.authList.add(authName);
	}
	
	public String getAuthor(){
		int index;
		int size=authList.size();
		for(index=0;index<size;index++){
			//return (authList.get(index));
			System.out.println("Book Author:" +authList.get(index));
			//System.out.println(size);
		}
		
		return null;
	}
	
}