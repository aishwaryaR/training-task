package com.domparser;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class MyErrorHandler implements ErrorHandler{
	public void warning(SAXParseException spe) throws SAXException {
	  System.out.println(" warning while parsing file" );
	}
	        
	public void error(SAXParseException spe) throws SAXException {
		System.out.println(" Error while parsing file" );
	}


	public void fatalError(SAXParseException spe) throws SAXException {
		System.out.println(" FatalError while parsing file Xml file missing tag" );
	}
		
}