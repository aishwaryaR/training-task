package com.domparser;

import java.io.*;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * @author aishwarya
 *
 */
public class DomParse {

	Document dom;

	ArrayList<BookBean> bookDetail = new ArrayList<BookBean>();
	BookBean obj = new BookBean();

	BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	
	
    
	/**
	 * @throws IOException
	 */
	public void getBook() throws IOException {				/* Method which calls the parsing methods */
		
		parseXmlFile();
		parseDocument();
	}

	
	/**
	 * 
	 */
	public void parseXmlFile() {									/* Method for parsing the xml file */
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			// Using factory get an instance of document builder
			DocumentBuilder db = dbf.newDocumentBuilder();
			dbf.setValidating(false);
			dbf.setNamespaceAware(true);
			db.setErrorHandler(new MyErrorHandler());

			// parse using builder to get DOM representation of the XML file
			dom = db.parse("xmlFiles/book.xml");

		} catch (ParserConfigurationException pce) {
			System.out.println("Parser Configuration Error");
		} catch (SAXException se) {
			System.out.println("Error while parsing the xml file");
		} catch (IOException ioe) {
			System.out.println("IO exception");
		}
	}


	/**
	 * @throws IOException
	 * @throws NullPointerException
	 */
	public void parseDocument() throws IOException, NullPointerException {		/* Method for getting the content from parsed document*/
		
		try {
			Element docEle = dom.getDocumentElement();
			System.out.println("Root element of the document: " + docEle.getNodeName());

			NodeList nl = docEle.getElementsByTagName("book");
			System.out.println("Total books: " + nl.getLength());

			if (nl != null && nl.getLength() > 0) {
					for (int i = 0; i < nl.getLength(); i++) {
						Node node = nl.item(i);
						if (node.getNodeType() == Node.ELEMENT_NODE) {
							Element e = (Element) node;
							printData(e);
					}
				}
			}
		} catch (NullPointerException npe) {
			System.out.println("Error in parsing xml Document");
		}
	}


	/**
	 * @param ele
	 * @throws IOException
	 */
	public void printData(Element ele) throws IOException {			/*Method for reading the content from xml file*/

		obj = new BookBean();
		
			if (ele.getAttribute("type") != "") {

			obj.setBookType(ele.getAttribute("type"));

			NodeList nodeList = ele.getElementsByTagName("author");

			for (int x = 0; x < nodeList.getLength(); x++) {
				obj.addAuthor(ele.getElementsByTagName("author").item(x).getTextContent());
			}

			nodeList = ele.getElementsByTagName("title");
			obj.setBookTitle(nodeList.item(0).getChildNodes().item(0).getNodeValue());

			nodeList = ele.getElementsByTagName("isbn");
			obj.setBookIsbn(nodeList.item(0).getChildNodes().item(0).getNodeValue());

		}
		bookDetail.add(obj);
		printBookList(obj);
	}
	
	
	/**
	 * @param title
	 */
	public void getBookDetails(String title) {			/* Method for searching books */
			int bookFound = 0;
			
		for (BookBean bookObj : bookDetail) {
			if (bookObj.getBookTitle().equals(title)) {
				bookFound++;
				 System.out.println("=========Book Found=========");
				 	printBookList(bookObj);
			}
		  } if (bookFound == 0) {
			System.out.println("No Such Book Avaliable");
		}
}
	
	
	/**
	 * @param type
	 * @throws IOException
	 */
	public void deleteBookDetails(String type) throws IOException {			/* Method for deleting the Books */
		   String check;
		   int bookTypeCount = 0;
		   Iterator<BookBean> bookListItr = bookDetail.iterator();
		
		for (BookBean typeObj : bookDetail) {
			if (typeObj.getBookType().equals(type)) {
				bookTypeCount++;
				printBookList(typeObj);
			}
		} 
			if (bookTypeCount == 0) {
			System.out.println("No such book is available");
		} else if (bookTypeCount == 1) {
			
					while (bookListItr.hasNext()) {
							if (bookListItr.next().getBookType().equals(type)) {
								System.out.println("Are you sure want to delete(Yes/No)");
								check = in.readLine();
	           	    		if (check.equalsIgnoreCase("yes")) {
	           	    			bookListItr.remove();
	           	    			System.out.println("Book deleted successfully");
	           	    	} else if (check.equalsIgnoreCase("No")) {
	           	    			System.out.println("Book is not deleted");
	           	    		}
	           	    	}
	           	    }
			} else if (bookTypeCount > 1) {
						String Isbn;

						System.out.println("One or more entries are there To delete the specific book give the book isbn:");
						Isbn = in.readLine();
			
						while (bookListItr.hasNext()) {
							if (bookListItr.next().getBookIsbn().equals(Isbn)) {
								System.out.println("Are you sure want to delete(Yes/No)");
								check = in.readLine();
							if (check.equalsIgnoreCase("yes")) {
								bookListItr.remove();
								System.out.println("Book deleted successfully");
           	    		} else if (check.equalsIgnoreCase("No")) {
           	    				System.out.println("Book is not deleted");
           	    		}
					} 
				}
		   }	
		 for (BookBean delBeanObj : bookDetail ) {
			 printBookList(delBeanObj);
		 }
	}
	
	
	/**
	 * @param booktype
	 * @param authlist
	 * @param booktitle
	 * @param bookisbn
	 */
	public void addBooks(String booktype, List<String> authlist, String booktitle, String bookisbn) {				/* Method for Adding NewBooks */
		
		int bookCount =0;
		BookBean newBook = new BookBean();

		newBook.setBookType(booktype);
		newBook.setAuthList(authlist);
		newBook.setBookTitle(booktitle);
		newBook.setBookIsbn(bookisbn);
		
		for (BookBean isbnObj : bookDetail) {
			if (isbnObj.getBookIsbn().equals(bookisbn)) {
				bookCount++;
				}
			}
			if (bookCount == 0) {
				bookDetail.add(newBook);
				System.out.println("Book added Successfully");
			}
			if (bookCount == 1) {
			System.out.println("Book Isbn should be unique number");
		} 
		for (BookBean newBookList : bookDetail)
		 printBookList(newBookList);
	}

	
	/**
	 * @throws IOException
	 */
	public void convertToXml() throws IOException {				/* Method for update the changes in the xml file */

		List<String> authobj;
		File file = new File("xmlFiles/book.xml");
		FileWriter fileWriter = new FileWriter(file);

		fileWriter.write("\n<booklist>");
			for (int m = 0; m < bookDetail.size(); m++) {
				fileWriter.write("\n<book type=\"" + bookDetail.get(m).getBookType() + "\">");

			authobj = bookDetail.get(m).getAuthList();
			for (int l = 0; l < authobj.size(); l++) {
				fileWriter.write("\n\t<author>" + authobj.get(l) + "</author>");
			}
			fileWriter.write("\n\t<title>" + bookDetail.get(m).getBookTitle()
					+ "</title>");
			fileWriter.write("\n\t<isbn>" + bookDetail.get(m).getBookIsbn()
					+ "</isbn>");
			fileWriter.write("\n</book>");

		}
		fileWriter.write("\n</booklist>");
		fileWriter.close();
	}
	
	
	/**
	 * @param beanobj
	 */
	public void printBookList(BookBean beanobj) {				 /* Method for printing the ArrayList*/
		
		System.out.println("\nBook Type:" + beanobj.getBookType());
		System.out.println("Book Author:" + beanobj.getAuthList());
		System.out.println("Book Title :" + beanobj.getBookTitle());
		System.out.println("Book ISBN  :" + beanobj.getBookIsbn());
		System.out.println("===============================");
	}

}