package com.domparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aishwarya
 *
 */
public class MainParse{

/**
 * @param args
 * @throws NumberFormatException
 * @throws IOException
 */
public static void main(String args[]) throws NumberFormatException,
			IOException {
	
   try{
		DomParse dm = new DomParse();
		dm.getBook();
		
		int n;
		
		do {
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("\n choose any of the option");
			System.out.println("\t\t 1.Find Book using Title.");
			System.out.println("\t\t 2.Delete Book using Type.");
			System.out.println("\t\t 3.Add  new  Books.");
			System.out.println("\t\t 4.Generate Xml File");
			System.out.println("\t\t 5.Exit.");
			System.out.println("\n Enter ur choice:");
			
			n = Integer.parseInt(bufferRead.readLine());

			String bookTitle;
			String bookType;

			String type;
			String author = null;
			String title;
			String isbn;

			switch (n) {
			case 1:
				System.out.println("\n Enter the Title:");
				bookTitle = bufferRead.readLine();
				
					if (bookTitle.isEmpty()) {
						System.out.println("Enter any book title");
					} else {
						dm.getBookDetails(bookTitle);				/* Method for searching books */
					}
				break;

			case 2:
				System.out.println("\n Enter the bookType: ");
				bookType = bufferRead.readLine();
				
					if (bookType.isEmpty()) {
						System.out.println("Enter bookType to delete");
					} else {
						dm.deleteBookDetails(bookType);				/* Method for deleting the Books */
					}
				break;

			case 3:
				System.out.println("\nEnter the book type:");
				type = bufferRead.readLine();

				System.out.println("\nEnter no.of.author:");
				n = Integer.parseInt(bufferRead.readLine());
				
				List<String> authorList = new ArrayList<String>();

					for (int k = 0; k < n; k++) {
						System.out.println("\nEnter the book auth:");
						author = bufferRead.readLine();
						authorList.add(author);
					}

				System.out.println("\nEnter the book Title:");
				title = bufferRead.readLine();

				System.out.println("\nEnter the book isbn:");
				isbn = bufferRead.readLine();
				
					if ((type.isEmpty() || author.isEmpty()) || (title.isEmpty() || isbn.isEmpty())) {
						System.out.println("All fields are mandatory");
					} else {
						dm.addBooks(type, authorList, title, isbn);			/* Method for Adding NewBooks */
					}
				break;
				
			case 4:
				dm.convertToXml();								/* Method for update the changes in the xml file */
					System.out.println("File Write Done");
				break;
				
			case 5:
				System.exit(0);

			default:
				System.out.println("Invalid entry ! Enter value btw 1 to 5.");
			}
			} while (n != 5);
		} catch (NumberFormatException numberformatexcep) {
				System.out.println("Enter any valid numbers");
		
		}
		
	}
}