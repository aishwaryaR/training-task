import java.util.Scanner;

class ReplaceString1
{
	public static String replaceString(String str, char ch) 
	    {
	    	int i;
	      
	        StringBuilder sb = new StringBuilder();
	    	
	  for(i=0;i<str.length();i++)
	    	{
		      if(str.charAt(i) == ch)
		    	   sb.append("o");
		       else
		    	    sb.append(str.charAt(i));
	    	}
	        
	  
	   return (sb.toString());
	
	    }
	
	public static void main(String[] args)
    {
        String originalString;
        char c = 'a' ;
        String result=null;
        
        Scanner inp = new Scanner(System.in);
        
        System.out.println("Enter the string:");
          
          originalString = inp.nextLine();
           
          result = replaceString(originalString, c);
           
          inp.close();
          
          if(result != null)
        	  System.out.println(result);
          else
        	  System.out.println("");
    }     
          
          
}        
        
