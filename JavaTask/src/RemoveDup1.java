import java.util.Scanner;

class RemoveDup1 {
	public static void main(String args[]) {
		int arr[] = new int[5];
		int newarr[] = new int[5];
		int i, j;
		int temp;

		Scanner inp = new Scanner(System.in);

		System.out.println("Enter the array values:");

		for (i = 0; i < arr.length; i++) {
			arr[i] = inp.nextInt();
		}

		for (i = 0; i < arr.length; i++) {
			for (j = i + 1; j < arr.length; j++) {
				if (arr[i] > arr[j]) {
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}

		}

		int b = 0;
		newarr[b] = arr[0];
		for (i = 1; i < arr.length; i++) {
			if (newarr[b] != arr[i]) {
				b++;
				newarr[b] = arr[i];

			}

		}

		inp.close();
		for (j = 0; j <= b; j++)
			System.out.println("Array values:" + newarr[j]);
	}

}